<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/authentication/library/ConstAuthentication.php');
include($strRootPath . '/src/authentication/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/authentication/api/AuthenticationInterface.php');
include($strRootPath . '/src/authentication/model/DefaultAuthentication.php');

include($strRootPath . '/src/authentication/secret/library/ConstSecretAuthentication.php');
include($strRootPath . '/src/authentication/secret/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/authentication/secret/model/SecretAuthentication.php');

include($strRootPath . '/src/authentication/token/library/ConstTokenAuthentication.php');
include($strRootPath . '/src/authentication/token/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/authentication/token/model/TokenAuthentication.php');

include($strRootPath . '/src/authentication/credential/library/ConstCredentialAuthentication.php');
include($strRootPath . '/src/authentication/credential/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/authentication/credential/model/CredentialAuthentication.php');

include($strRootPath . '/src/authenticator/library/ConstAuthenticator.php');
include($strRootPath . '/src/authenticator/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/authenticator/api/AuthenticatorInterface.php');
include($strRootPath . '/src/authenticator/model/DefaultAuthenticator.php');

include($strRootPath . '/src/agent/library/ConstAgent.php');
include($strRootPath . '/src/agent/exception/AuthenticationInvalidFormatException.php');
include($strRootPath . '/src/agent/exception/AuthenticatorInvalidFormatException.php');
include($strRootPath . '/src/agent/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/agent/api/AgentInterface.php');
include($strRootPath . '/src/agent/model/DefaultAgent.php');

include($strRootPath . '/src/agent/fix/library/ConstFixAgent.php');
include($strRootPath . '/src/agent/fix/exception/AuthenticationInvalidFormatException.php');
include($strRootPath . '/src/agent/fix/exception/AuthenticatorInvalidFormatException.php');
include($strRootPath . '/src/agent/fix/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/agent/fix/model/FixAgent.php');