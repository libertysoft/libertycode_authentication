<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\authentication\secret\exception;

use liberty_code\authentication\authentication\secret\library\ConstSecretAuthentication;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstSecretAuthentication::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid identifier
            isset($config[ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER]) &&
            is_string($config[ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER]) &&
            (trim($config[ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER]) != '') &&

            // Check valid identifier REGEXP pattern selection
            (
                (!isset($config[ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER_REGEXP_SELECT])) ||
                (
                    is_string($config[ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER_REGEXP_SELECT]) &&
                    (trim($config[ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER_REGEXP_SELECT]) != '')
                )
            ) &&

            // Check valid secret
            (
                (!isset($config[ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET])) ||
                is_string($config[ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET])
            ) &&

            // Check valid secret REGEXP pattern selection
            (
                (!isset($config[ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET_REGEXP_SELECT])) ||
                (
                    is_string($config[ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET_REGEXP_SELECT]) &&
                    (trim($config[ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET_REGEXP_SELECT]) != '')
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}