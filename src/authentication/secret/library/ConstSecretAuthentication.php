<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\authentication\secret\library;



class ConstSecretAuthentication
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_IDENTIFIER = 'identifier';
    const TAB_CONFIG_KEY_IDENTIFIER_REGEXP_SELECT = 'identifier_regexp_select';
    const TAB_CONFIG_KEY_SECRET = 'secret';
    const TAB_CONFIG_KEY_SECRET_REGEXP_SELECT = 'secret_regexp_select';

    // Data configuration
    const TAB_DATA_KEY_IDENTIFIER = 'identifier';
    const TAB_DATA_KEY_SECRET = 'secret';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the secret authentication configuration standard.';
}