<?php
/**
 * This class allows to define secret authentication class.
 * Secret authentication uses identifier, as identification information
 * and secret, as authentication information.
 *
 * Secret authentication uses the following specified configuration:
 * [
 *     identifier(required): "string identifier",
 *
 *     identifier_regexp_select(optional): "string REGEXP pattern, where first selection is considered as identifier",
 *
 *     secret(optional: got null if not found): "string secret",
 *
 *     secret_regexp_select(optional): "string REGEXP pattern, where first selection is considered as secret"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\authentication\secret\model;

use liberty_code\authentication\authentication\model\DefaultAuthentication;

use liberty_code\authentication\authentication\library\ConstAuthentication;
use liberty_code\authentication\authentication\secret\library\ConstSecretAuthentication;
use liberty_code\authentication\authentication\secret\exception\ConfigInvalidFormatException;



class SecretAuthentication extends DefaultAuthentication
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAuthentication::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get identifier.
     *
     * @return string
     */
    public function getStrIdentifier()
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();
        $result = $tabConfig[ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER];

        // Select identifier, if required
        if(isset($tabConfig[ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER_REGEXP_SELECT]))
        {
            $tabMatch = array();
            $strPattern = $tabConfig[ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER_REGEXP_SELECT];
            $result = (
                (
                    (preg_match($strPattern, $result, $tabMatch) !== false) &&
                    isset($tabMatch[1])
                ) ?
                    $tabMatch[1] :
                    $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get secret.
     *
     * @return null|string
     */
    public function getStrSecret()
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();
        $result = (
            isset($tabConfig[ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET]) ?
                $tabConfig[ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET] :
                null
        );

        // Select secret, if required
        if(isset($tabConfig[ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET_REGEXP_SELECT]))
        {
            $tabMatch = array();
            $strPattern = $tabConfig[ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET_REGEXP_SELECT];
            $result = (
            (
                (preg_match($strPattern, $result, $tabMatch) !== false) &&
                isset($tabMatch[1])
            ) ?
                $tabMatch[1] :
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return string[]
     */
    public function getTabIdDataEngine()
    {
        // Return result
        return array(
            ConstSecretAuthentication::TAB_DATA_KEY_IDENTIFIER => $this->getStrIdentifier()
        );
    }



    /**
     * @inheritdoc
     * @return string[]
     */
    public function getTabAuthDataEngine()
    {
        // Return result
        return array(
            ConstSecretAuthentication::TAB_DATA_KEY_SECRET => $this->getStrSecret()
        );
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set identifier.
     *
     * @param string $strIdentifier
     */
    public function setIdentifier($strIdentifier)
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();

        // Set identifier
        $tabConfig[ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER] = $strIdentifier;

        // Set configuration
        $this->setAuthConfig($tabConfig);
    }



    /**
     * Set secret.
     *
     * @param null|string $strSecret
     */
    public function setSecret($strSecret)
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();

        // Set secret
        $tabConfig[ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET] = $strSecret;

        // Set configuration
        $this->setAuthConfig($tabConfig);
    }



}