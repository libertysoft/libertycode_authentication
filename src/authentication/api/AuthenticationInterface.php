<?php
/**
 * Description :
 * This class allows to describe behavior of authentication class.
 * Authentication allows to get all identification and authentication information,
 * about one specific entity.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\authentication\api;



interface AuthenticationInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

	/**
	 * Get associative array of identification data.
	 *
	 * @return mixed[]
	 */
	public function getTabIdData();



    /**
     * Get associative array of authentication data.
     *
     * @return mixed[]
     */
    public function getTabAuthData();



    /**
     * Get authentication configuration array.
     *
     * @return array
     */
    public function getTabAuthConfig();





    // Methods setters
    // ******************************************************************************

    /**
     * Set authentication configuration array.
     *
     * @param array $tabConfig
     */
    public function setAuthConfig(array $tabConfig);
}