<?php
/**
 * This class allows to define credential authentication class.
 * Credential authentication uses credential to retrieve identifier, as identification information
 * and secret, as authentication information.
 *
 * Secret authentication uses the following specified configuration:
 * [
 *     credential(required): "string credential",
 *
 *     identifier_regexp_select(optional): "string REGEXP pattern, where first selection is considered as identifier",
 *
 *     secret_regexp_select(optional): "string REGEXP pattern, where first selection is considered as secret"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\authentication\credential\model;

use liberty_code\authentication\authentication\model\DefaultAuthentication;

use liberty_code\authentication\authentication\library\ConstAuthentication;
use liberty_code\authentication\authentication\credential\library\ConstCredentialAuthentication;
use liberty_code\authentication\authentication\credential\exception\ConfigInvalidFormatException;



class CredentialAuthentication extends DefaultAuthentication
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAuthentication::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get credential.
     *
     * @return string
     */
    public function getStrCredential()
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();
        $result = $tabConfig[ConstCredentialAuthentication::TAB_CONFIG_KEY_CREDENTIAL];

        // Return result
        return $result;
    }



    /**
     * Get identifier.
     * Considered credential is identifier,
     * if identifier REGEXP pattern selection not found.
     *
     * @return string
     */
    public function getStrIdentifier()
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();
        $result = $this->getStrCredential();

        // Select identifier, if required
        if(isset($tabConfig[ConstCredentialAuthentication::TAB_CONFIG_KEY_IDENTIFIER_REGEXP_SELECT]))
        {
            $tabMatch = array();
            $strPattern = $tabConfig[ConstCredentialAuthentication::TAB_CONFIG_KEY_IDENTIFIER_REGEXP_SELECT];
            $result = (
                (
                    (preg_match($strPattern, $result, $tabMatch) !== false) &&
                    isset($tabMatch[1])
                ) ?
                    $tabMatch[1] :
                    $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get secret.
     *
     * @return null|string
     */
    public function getStrSecret()
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();
        $result = null;

        // Select secret, if required
        if(isset($tabConfig[ConstCredentialAuthentication::TAB_CONFIG_KEY_SECRET_REGEXP_SELECT]))
        {
            $tabMatch = array();
            $strPattern = $tabConfig[ConstCredentialAuthentication::TAB_CONFIG_KEY_SECRET_REGEXP_SELECT];
            $strCredential = $this->getStrCredential();
            $result = (
                (
                    (preg_match($strPattern, $strCredential, $tabMatch) !== false) &&
                    isset($tabMatch[1])
                ) ?
                    $tabMatch[1] :
                    $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return string[]
     */
    public function getTabIdDataEngine()
    {
        // Return result
        return array(
            ConstCredentialAuthentication::TAB_DATA_KEY_IDENTIFIER => $this->getStrIdentifier()
        );
    }



    /**
     * @inheritdoc
     * @return string[]
     */
    public function getTabAuthDataEngine()
    {
        // Return result
        return array(
            ConstCredentialAuthentication::TAB_DATA_KEY_SECRET => $this->getStrSecret()
        );
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set credential.
     *
     * @param string $strCredential
     */
    public function setCredential($strCredential)
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();

        // Set identifier
        $tabConfig[ConstCredentialAuthentication::TAB_CONFIG_KEY_CREDENTIAL] = $strCredential;

        // Set configuration
        $this->setAuthConfig($tabConfig);
    }



}