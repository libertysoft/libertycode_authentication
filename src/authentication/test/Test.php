<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\authentication\authentication\secret\library\ConstSecretAuthentication;
use liberty_code\authentication\authentication\secret\model\SecretAuthentication;
use liberty_code\authentication\authentication\token\library\ConstTokenAuthentication;
use liberty_code\authentication\authentication\token\model\TokenAuthentication;
use liberty_code\authentication\authentication\credential\library\ConstCredentialAuthentication;
use liberty_code\authentication\authentication\credential\model\CredentialAuthentication;



// Init var
$objSecretAuth = new SecretAuthentication();
$objTokenAuth = new TokenAuthentication();
$objCredentialAuth = new CredentialAuthentication();



// Test secret authentication
$tabTabConfig = array(
    [
        ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER => 7,
        ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET => 'pw'
    ], // Ko: Identifier bad format

    [
        ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER => 'user-1',
        ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET => 7
    ], // Ko: Secret bad format

    [
        ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER => 'user-1',
        ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET => 'pw'
    ], // Ok

    [
        ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET => 'pw'
    ], // Ko: Identifier not found

    [
        ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER => 'user-1'
    ] // Ok
);

foreach($tabTabConfig as $tabConfig)
{
    echo('Test secret authentication : <pre>');
    var_dump($tabConfig);
    echo('</pre>');

    try{
        $objSecretAuth->setAuthConfig($tabConfig);

        echo('Get configuration: <pre>');var_dump($objSecretAuth->getTabAuthConfig());echo('</pre>');
        echo('Get identification: <pre>');var_dump($objSecretAuth->getTabIdData());echo('</pre>');
        echo('Get authentication: <pre>');var_dump($objSecretAuth->getTabAuthData());echo('</pre>');
        echo('Get identifier: <pre>');var_dump($objSecretAuth->getStrIdentifier());echo('</pre>');
        echo('Get secret: <pre>');var_dump($objSecretAuth->getStrSecret());echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test token authentication
$tabTabConfig = array(
    [
        ConstTokenAuthentication::TAB_CONFIG_KEY_TOKEN => 7,
    ], // Ko: Token bad format

    [
        ConstTokenAuthentication::TAB_CONFIG_KEY_TOKEN => 'token-1'
    ], // Ok

    [], // Ko: Token not found

    [
        ConstTokenAuthentication::TAB_CONFIG_KEY_TOKEN => 'Bearer token-1',
        ConstTokenAuthentication::TAB_CONFIG_KEY_TOKEN_REGEXP_SELECT => '#^Bearer (.+)$#'
    ], // Ok
);

foreach($tabTabConfig as $tabConfig)
{
    echo('Test token authentication : <pre>');
    var_dump($tabConfig);
    echo('</pre>');

    try{
        $objTokenAuth->setAuthConfig($tabConfig);

        echo('Get configuration: <pre>');var_dump($objTokenAuth->getTabAuthConfig());echo('</pre>');
        echo('Get identification: <pre>');var_dump($objTokenAuth->getTabIdData());echo('</pre>');
        echo('Get authentication: <pre>');var_dump($objTokenAuth->getTabAuthData());echo('</pre>');
        echo('Get token: <pre>');var_dump($objTokenAuth->getStrToken());echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test credential authentication
$tabTabConfig = array(
    [
        ConstCredentialAuthentication::TAB_CONFIG_KEY_CREDENTIAL => 7,
    ], // Ko: Credential bad format

    [
        ConstCredentialAuthentication::TAB_CONFIG_KEY_CREDENTIAL => 'user-1'
    ], // Ok

    [], // Ko: Credential not found

    [
        ConstCredentialAuthentication::TAB_CONFIG_KEY_CREDENTIAL => 'Basic user-1:pw',
        ConstCredentialAuthentication::TAB_CONFIG_KEY_IDENTIFIER_REGEXP_SELECT => '#^Basic (.+):.+$#',
        ConstCredentialAuthentication::TAB_CONFIG_KEY_SECRET_REGEXP_SELECT => '#^Basic .+:(.+)$#'
    ], // Ok
);

foreach($tabTabConfig as $tabConfig)
{
    echo('Test credential authentication : <pre>');
    var_dump($tabConfig);
    echo('</pre>');

    try{
        $objCredentialAuth->setAuthConfig($tabConfig);

        echo('Get configuration: <pre>');var_dump($objCredentialAuth->getTabAuthConfig());echo('</pre>');
        echo('Get identification: <pre>');var_dump($objCredentialAuth->getTabIdData());echo('</pre>');
        echo('Get authentication: <pre>');var_dump($objCredentialAuth->getTabAuthData());echo('</pre>');
        echo('Get credential: <pre>');var_dump($objCredentialAuth->getStrCredential());echo('</pre>');
        echo('Get identifier: <pre>');var_dump($objCredentialAuth->getStrIdentifier());echo('</pre>');
        echo('Get secret: <pre>');var_dump($objCredentialAuth->getStrSecret());echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


