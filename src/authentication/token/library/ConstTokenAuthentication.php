<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\authentication\token\library;



class ConstTokenAuthentication
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_TOKEN = 'token';
    const TAB_CONFIG_KEY_TOKEN_REGEXP_SELECT = 'token_regexp_select';

    // Data configuration
    const TAB_DATA_KEY_TOKEN = 'token';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the token authentication configuration standard.';
}