<?php
/**
 * This class allows to define token authentication class.
 * Token authentication uses token, as identification and authentication information.
 *
 * Token authentication uses the following specified configuration:
 * [
 *     token(required): "string token",
 *
 *     token_regexp_select(optional): "string REGEXP pattern, where first selection is considered as token"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\authentication\token\model;

use liberty_code\authentication\authentication\model\DefaultAuthentication;

use liberty_code\authentication\authentication\library\ConstAuthentication;
use liberty_code\authentication\authentication\token\library\ConstTokenAuthentication;
use liberty_code\authentication\authentication\token\exception\ConfigInvalidFormatException;



class TokenAuthentication extends DefaultAuthentication
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAuthentication::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get token.
     *
     * @return string
     */
    public function getStrToken()
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();
        $result = $tabConfig[ConstTokenAuthentication::TAB_CONFIG_KEY_TOKEN];

        // Select token, if required
        if(isset($tabConfig[ConstTokenAuthentication::TAB_CONFIG_KEY_TOKEN_REGEXP_SELECT]))
        {
            $tabMatch = array();
            $strPattern = $tabConfig[ConstTokenAuthentication::TAB_CONFIG_KEY_TOKEN_REGEXP_SELECT];
            $result = (
            (
                (preg_match($strPattern, $result, $tabMatch) !== false) &&
                isset($tabMatch[1])
            ) ?
                $tabMatch[1] :
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return string[]
     */
    public function getTabIdDataEngine()
    {
        // Return result
        return array(
            ConstTokenAuthentication::TAB_DATA_KEY_TOKEN => $this->getStrToken()
        );
    }



    /**
     * @inheritdoc
     * @return string[]
     */
    public function getTabAuthDataEngine()
    {
        // Return result
        return array(
            ConstTokenAuthentication::TAB_DATA_KEY_TOKEN => $this->getStrToken()
        );
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set token.
     *
     * @param string $strToken
     */
    public function setToken($strToken)
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();

        // Set token
        $tabConfig[ConstTokenAuthentication::TAB_CONFIG_KEY_TOKEN] = $strToken;

        // Set configuration
        $this->setAuthConfig($tabConfig);
    }



}