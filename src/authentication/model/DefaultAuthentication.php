<?php
/**
 * This class allows to define default authentication class.
 * Can be consider is base of all authentication types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\authentication\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\authentication\authentication\api\AuthenticationInterface;

use liberty_code\authentication\authentication\library\ConstAuthentication;
use liberty_code\authentication\authentication\exception\ConfigInvalidFormatException;



abstract class DefaultAuthentication extends FixBean implements AuthenticationInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     */
    public function __construct(array $tabConfig = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setAuthConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstAuthentication::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstAuthentication::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstAuthentication::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAuthentication::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get associative array of identification data engine.
     *
     * @return mixed[]
     */
    abstract protected function getTabIdDataEngine();



    /**
     * Get specified identification formatted data.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    protected function getIdDataFormat($strKey, $value)
    {
        // Format by data key
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabIdData()
    {
        // Init var
        $result = $this->getTabIdDataEngine();
        array_walk(
            $result,
            function(&$value, $strKey) {
                $value = $this->getIdDataFormat($strKey, $value);
            }
        );

        // Return result
        return $result;
    }



    /**
     * Get associative array of authentication data engine.
     *
     * @return mixed[]
     */
    abstract protected function getTabAuthDataEngine();



    /**
     * Get specified authentication formatted data.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    protected function getAuthDataFormat($strKey, $value)
    {
        // Format by data key
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabAuthData()
    {
        // Init var
        $result = $this->getTabAuthDataEngine();
        array_walk(
            $result,
            function(&$value, $strKey) {
                $value = $this->getAuthDataFormat($strKey, $value);
            }
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabAuthConfig()
    {
        // Return result
        return $this->beanGet(ConstAuthentication::DATA_KEY_DEFAULT_CONFIG);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setAuthConfig(array $tabConfig)
    {
        $this->beanSet(ConstAuthentication::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



}