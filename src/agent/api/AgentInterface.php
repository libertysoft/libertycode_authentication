<?php
/**
 * Description :
 * This class allows to describe behavior of agent class.
 * Agent is an authentication representative about one specific entity.
 * It uses authentication and authenticator,
 * to check its identification and its authentication.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\agent\api;

use liberty_code\authentication\authentication\api\AuthenticationInterface;
use liberty_code\authentication\authenticator\api\AuthenticatorInterface;



interface AgentInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods check
	// ******************************************************************************

    /**
     * Check is identified.
     *
     * @return boolean
     */
    public function checkIsIdentified();



    /**
     * Check is authenticated.
     *
     * @return boolean
     */
    public function checkIsAuthenticated();





    // Methods getters
    // ******************************************************************************

    /**
     * Get authentication object.
     *
     * @return AuthenticationInterface
     */
    public function getObjAuthentication();



    /**
     * Get authenticator object.
     *
     * @return AuthenticatorInterface
     */
    public function getObjAuthenticator();



    /**
     * Get string key (considered as agent id).
     *
     * @return string
     */
    public function getStrKey();





    // Methods setters
    // ******************************************************************************

    /**
     * Set authentication object.
     *
     * @param AuthenticationInterface $objAuthentication
     */
    public function setAuthentication(AuthenticationInterface $objAuthentication);



    /**
     * Set authenticator object.
     *
     * @param AuthenticatorInterface $objAuthenticator
     */
    public function setAuthenticator(AuthenticatorInterface $objAuthenticator);
}