<?php
/**
 * This class allows to define default agent class.
 * Can be consider is base of all agent types.
 *
 * Default agent uses the following specified configuration:
 * [
 *     key(optional: hash got if not found): "string route key",
 *
 *     cache_require(optional: got true if not found): true / false,
 *
 *     authentication(optional): [
 *         Authentication configuration (@see AuthenticationInterface::getTabAuthConfig() )
 *     ],
 *
 *     authenticator(optional): [
 *         Authenticator configuration (@see AuthenticatorInterface::getTabAuthConfig() )
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\agent\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\authentication\agent\api\AgentInterface;

use liberty_code\library\reflection\library\ToolBoxClassReflection;
use liberty_code\authentication\authentication\api\AuthenticationInterface;
use liberty_code\authentication\authenticator\api\AuthenticatorInterface;
use liberty_code\authentication\agent\library\ConstAgent;
use liberty_code\authentication\agent\exception\AuthenticationInvalidFormatException;
use liberty_code\authentication\agent\exception\AuthenticatorInvalidFormatException;
use liberty_code\authentication\agent\exception\ConfigInvalidFormatException;



/**
 * @method array getTabConfig() Get configuration array.
 * @method void setTabConfig(array $tabConfig) Set configuration array.
 */
class DefaultAgent extends FixBean implements AgentInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /** @var null|boolean */
    protected $boolCacheIsIdentified;



    /** @var null|boolean */
    protected $boolCacheIsAuthenticated;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AuthenticationInterface $objAuthentication
     * @param AuthenticatorInterface $objAuthenticator
     * @param array $tabConfig = null
     */
    public function __construct(
        AuthenticationInterface $objAuthentication,
        AuthenticatorInterface $objAuthenticator,
        array $tabConfig = null
    )
    {
        // Init var
        $this->boolCacheIsIdentified = null;
        $this->boolCacheIsAuthenticated = null;

        // Call parent constructor
        parent::__construct();

        // Init authentication
        $this->setAuthentication($objAuthentication);

        // Init authenticator
        $this->setAuthenticator($objAuthenticator);

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstAgent::DATA_KEY_DEFAULT_AUTHENTICATION))
        {
            $this->__beanTabData[ConstAgent::DATA_KEY_DEFAULT_AUTHENTICATION] = null;
        }

        if(!$this->beanExists(ConstAgent::DATA_KEY_DEFAULT_AUTHENTICATOR))
        {
            $this->__beanTabData[ConstAgent::DATA_KEY_DEFAULT_AUTHENTICATOR] = null;
        }

        if(!$this->beanExists(ConstAgent::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstAgent::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstAgent::DATA_KEY_DEFAULT_AUTHENTICATION,
            ConstAgent::DATA_KEY_DEFAULT_AUTHENTICATOR,
            ConstAgent::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAgent::DATA_KEY_DEFAULT_AUTHENTICATION:
                    AuthenticationInvalidFormatException::setCheck($value);
                    break;

                case ConstAgent::DATA_KEY_DEFAULT_AUTHENTICATOR:
                    AuthenticatorInvalidFormatException::setCheck($value);
                    break;

                case ConstAgent::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check cache required.
     *
     * @return boolean
     */
    protected function checkCacheRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!array_key_exists(ConstAgent::TAB_CONFIG_KEY_CACHE_REQUIRE, $tabConfig)) ||
            (intval($tabConfig[ConstAgent::TAB_CONFIG_KEY_CACHE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkIsIdentified()
    {
        // Init var
        $result = $this->boolCacheIsIdentified;
        $boolCacheRequired = $this->checkCacheRequired();

        // Check is identified, if required
        if((!$boolCacheRequired) || (is_null($result))) {
            // Init var
            $objAuthentication = $this->getObjAuthentication();
            $objAuthenticator = $this->getObjAuthenticator();

            // Check identification
            $result = $objAuthenticator->checkIsIdentified($objAuthentication);

            // Set cache result
            $this->boolCacheIsIdentified = (
                ($boolCacheRequired) ?
                    $result :
                    null
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkIsAuthenticated()
    {
        // Init var
        $result = $this->boolCacheIsAuthenticated;
        $boolCacheRequired = $this->checkCacheRequired();

        // Check is authenticated, if required
        if((!$boolCacheRequired) || (is_null($result))) {
            // Init var
            $objAuthentication = $this->getObjAuthentication();
            $objAuthenticator = $this->getObjAuthenticator();

            // Check authentication
            $result = $objAuthenticator->checkIsAuthenticated($objAuthentication);

            // Set cache result
            $this->boolCacheIsAuthenticated = (
                ($boolCacheRequired) ?
                    $result :
                    null
            );
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getObjAuthentication()
    {
        // Return result
        return $this->beanGet(ConstAgent::DATA_KEY_DEFAULT_AUTHENTICATION);
    }



    /**
     * @inheritdoc
     */
    public function getObjAuthenticator()
    {
        // Return result
        return $this->beanGet(ConstAgent::DATA_KEY_DEFAULT_AUTHENTICATOR);
    }



    /**
     * @inheritdoc
     */
    public function getStrKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstAgent::TAB_CONFIG_KEY_KEY, $tabConfig) ?
                $tabConfig[ConstAgent::TAB_CONFIG_KEY_KEY] :
                ToolBoxClassReflection::getStrClassName(static::class)
        );

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Remove cache.
     */
    public function removeCache()
    {
        $this->boolCacheIsIdentified = null;
        $this->boolCacheIsAuthenticated = null;
    }



    /**
     * Set authentication configuration array.
     */
    public function setAuthenticationConfig()
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Init configuration, if required
        if(isset($tabConfig[ConstAgent::TAB_CONFIG_KEY_AUTHENTICATION]))
        {
            $tabAuthConfig = $tabConfig[ConstAgent::TAB_CONFIG_KEY_AUTHENTICATION];
            $objAuthentication = $this->getObjAuthentication();
            $objAuthentication->setAuthConfig($tabAuthConfig);
        }
    }



    /**
     * Set authenticator configuration array.
     */
    public function setAuthenticatorConfig()
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Init configuration, if required
        if(isset($tabConfig[ConstAgent::TAB_CONFIG_KEY_AUTHENTICATOR]))
        {
            $tabAuthConfig = $tabConfig[ConstAgent::TAB_CONFIG_KEY_AUTHENTICATOR];
            $objAuthenticator = $this->getObjAuthenticator();
            $objAuthenticator->setAuthConfig($tabAuthConfig);
        }
    }



    /**
     * @inheritdoc
     */
    protected function beanSet($key, $val, $boolTranslate = false)
    {
        // Call parent method
        parent::beanSet($key, $val, $boolTranslate);

        // Remove cache
        $this->removeCache();

        // Set configuration
        $this->setAuthenticationConfig();
        $this->setAuthenticatorConfig();
    }



    /**
     * @inheritdoc
     */
    public function setAuthentication(AuthenticationInterface $objAuthentication)
    {
        $this->beanSet(ConstAgent::DATA_KEY_DEFAULT_AUTHENTICATION, $objAuthentication);
    }



    /**
     * @inheritdoc
     */
    public function setAuthenticator(AuthenticatorInterface $objAuthenticator)
    {
        $this->beanSet(ConstAgent::DATA_KEY_DEFAULT_AUTHENTICATOR, $objAuthenticator);
    }



}