<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\agent\exception;

use liberty_code\authentication\agent\library\ConstAgent;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstAgent::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid key
            (
                (!isset($config[ConstAgent::TAB_CONFIG_KEY_KEY])) ||
                (
                    is_string($config[ConstAgent::TAB_CONFIG_KEY_KEY]) &&
                    (trim($config[ConstAgent::TAB_CONFIG_KEY_KEY]) != '')
                )
            ) &&

            // Check valid cache required option
            (
                (!isset($config[ConstAgent::TAB_CONFIG_KEY_CACHE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstAgent::TAB_CONFIG_KEY_CACHE_REQUIRE]) ||
                    is_int($config[ConstAgent::TAB_CONFIG_KEY_CACHE_REQUIRE]) ||
                    (
                        is_string($config[ConstAgent::TAB_CONFIG_KEY_CACHE_REQUIRE]) &&
                        ctype_digit($config[ConstAgent::TAB_CONFIG_KEY_CACHE_REQUIRE])
                    )
                )
            ) &&

            // Check valid authentication configuration
            (
                (!isset($config[ConstAgent::TAB_CONFIG_KEY_AUTHENTICATION])) ||
                is_array($config[ConstAgent::TAB_CONFIG_KEY_AUTHENTICATION])
            ) &&

            // Check valid authenticator configuration
            (
                (!isset($config[ConstAgent::TAB_CONFIG_KEY_AUTHENTICATOR])) ||
                is_array($config[ConstAgent::TAB_CONFIG_KEY_AUTHENTICATOR])
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}