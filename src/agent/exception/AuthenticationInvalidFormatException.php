<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\agent\exception;

use liberty_code\authentication\authentication\api\AuthenticationInterface;
use liberty_code\authentication\agent\library\ConstAgent;



class AuthenticationInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $authentication
     */
	public function __construct($authentication)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstAgent::EXCEPT_MSG_AUTHENTICATION_INVALID_FORMAT,
            mb_strimwidth(strval($authentication), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified authentication has valid format.
	 * 
     * @param mixed $authentication
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($authentication)
    {
		// Init var
		$result = (
			(is_null($authentication)) ||
			($authentication instanceof AuthenticationInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($authentication);
		}
		
		// Return result
		return $result;
    }
	
	
	
}