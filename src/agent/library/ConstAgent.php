<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\agent\library;



class ConstAgent
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_AUTHENTICATION = 'objAuthentication';
    const DATA_KEY_DEFAULT_AUTHENTICATOR = 'objAuthenticator';
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_KEY = 'key';
    const TAB_CONFIG_KEY_CACHE_REQUIRE = 'cache_require';
    const TAB_CONFIG_KEY_AUTHENTICATION = 'authentication';
    const TAB_CONFIG_KEY_AUTHENTICATOR = 'authenticator';



    // Exception message constants
    const EXCEPT_MSG_AUTHENTICATION_INVALID_FORMAT = 'Following authentication "%1$s" invalid! It must be an authentication object.';
    const EXCEPT_MSG_AUTHENTICATOR_INVALID_FORMAT = 'Following authenticator "%1$s" invalid! It must be an authenticator object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default agent configuration standard.';
}