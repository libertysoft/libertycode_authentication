<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\agent\fix\library;



class ConstFixAgent
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Exception message constants
    const EXCEPT_MSG_AUTHENTICATION_INVALID_FORMAT = 'Following authentication "%1$s" invalid! It must be a valid authentication object.';
    const EXCEPT_MSG_AUTHENTICATOR_INVALID_FORMAT = 'Following authenticator "%1$s" invalid! It must be a valid authenticator object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the fixed agent configuration standard.';
}