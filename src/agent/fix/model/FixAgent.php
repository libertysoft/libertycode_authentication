<?php
/**
 * Description :
 * This class allows to define fixed agent class.
 * Fixed agent allows to check identification and authentication, about one specific entity,
 * from a specified fixed configuration.
 *
 * Fixed agent allows to specify parts of configuration:
 * [
 *     Default agent configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\agent\fix\model;

use liberty_code\authentication\agent\model\DefaultAgent;

use liberty_code\authentication\authentication\api\AuthenticationInterface;
use liberty_code\authentication\authenticator\api\AuthenticatorInterface;
use liberty_code\authentication\agent\library\ConstAgent;
use liberty_code\authentication\agent\fix\exception\AuthenticationInvalidFormatException;
use liberty_code\authentication\agent\fix\exception\AuthenticatorInvalidFormatException;
use liberty_code\authentication\agent\fix\exception\ConfigInvalidFormatException;



abstract class FixAgent extends DefaultAgent
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(
        AuthenticationInterface $objAuthentication,
        AuthenticatorInterface $objAuthenticator
    )
    {
        // Init var
        $tabConfig = $this->getTabFixConfig();

        // Call parent constructor
        parent::__construct($objAuthentication, $objAuthenticator, $tabConfig);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAgent::DATA_KEY_DEFAULT_AUTHENTICATION:
                    $strFixClassPath = $this->getStrFixAuthenticationClassPath();
                    AuthenticationInvalidFormatException::setCheck($value, $strFixClassPath);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                case ConstAgent::DATA_KEY_DEFAULT_AUTHENTICATOR:
                    $strFixClassPath = $this->getStrFixAuthenticatorClassPath();
                    AuthenticatorInvalidFormatException::setCheck($value, $strFixClassPath);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                case ConstAgent::DATA_KEY_DEFAULT_CONFIG:
                    $tabFixConfig = $this->getTabFixConfig();
                    ConfigInvalidFormatException::setCheck($value, $tabFixConfig);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get fixed class path of authentication.
     * Null means no specific class path fixed.
     * Overwrite it to implement specific class path.
     *
     * @return null|string
     */
    protected function getStrFixAuthenticationClassPath()
    {
        // Return result
        return null;
    }



    /**
     * Get fixed class path of authenticator.
     * Null means no specific class path fixed.
     * Overwrite it to implement specific class path.
     *
     * @return null|string
     */
    protected function getStrFixAuthenticatorClassPath()
    {
        // Return result
        return null;
    }



    /**
     * Get fixed configuration array.
     *
     * @return array
     */
    abstract protected function getTabFixConfig();



}