<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\agent\fix\exception;

use liberty_code\authentication\agent\fix\library\ConstFixAgent;



class AuthenticatorInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $authenticator
     */
	public function __construct($authenticator)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstFixAgent::EXCEPT_MSG_AUTHENTICATOR_INVALID_FORMAT,
            mb_strimwidth(strval($authenticator), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified authenticator has valid format.
	 * 
     * @param mixed $authenticator
     * @param string $strFixClassPath = null
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($authenticator, $strFixClassPath = null)
    {
        // Init var
        $result =
            // Check specified class path, not required
            is_null($strFixClassPath) ||

            // Check specified class path, required
            ($authenticator instanceof $strFixClassPath);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static($authenticator);
        }
		
		// Return result
		return $result;
    }
	
	
	
}