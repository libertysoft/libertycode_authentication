<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\agent\fix\exception;

use liberty_code\authentication\agent\fix\library\ConstFixAgent;



class AuthenticationInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $authentication
     */
	public function __construct($authentication)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstFixAgent::EXCEPT_MSG_AUTHENTICATION_INVALID_FORMAT,
            mb_strimwidth(strval($authentication), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified authentication has valid format.
	 * 
     * @param mixed $authentication
     * @param string $strFixClassPath = null
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($authentication, $strFixClassPath = null)
    {
        // Init var
        $result =
            // Check specified class path, not required
            is_null($strFixClassPath) ||

            // Check specified class path, required
            ($authentication instanceof $strFixClassPath);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static($authentication);
        }
		
		// Return result
		return $result;
    }
	
	
	
}