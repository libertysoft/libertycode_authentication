<?php

namespace liberty_code\authentication\agent\test;

use liberty_code\authentication\agent\fix\model\FixAgent;

use liberty_code\authentication\authentication\secret\model\SecretAuthentication;
use liberty_code\authentication\authenticator\test\TestAuthenticator;
use liberty_code\authentication\agent\library\ConstAgent;



/**
 * @method SecretAuthentication getObjAuthentication() @inheritdoc
 * @method TestAuthenticator getObjAuthenticator() @inheritdoc
 */
class TestUserAgent extends FixAgent
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SecretAuthentication $objAuthentication
     * @param TestAuthenticator $objAuthenticator
     */
    public function __construct(
        SecretAuthentication $objAuthentication,
        TestAuthenticator $objAuthenticator
    )
    {
        // Call parent constructor
        parent::__construct($objAuthentication, $objAuthenticator);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixAuthenticationClassPath()
    {
        // Return result
        return SecretAuthentication::class;
    }



    /**
     * @inheritdoc
     */
    protected function getStrFixAuthenticatorClassPath()
    {
        // Return result
        return TestAuthenticator::class;
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstAgent::TAB_CONFIG_KEY_KEY => 'user',
            ConstAgent::TAB_CONFIG_KEY_CACHE_REQUIRE => true
        );
    }



}