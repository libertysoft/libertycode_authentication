<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/agent/test/AgentTest.php');

// Use
use liberty_code\authentication\authentication\secret\library\ConstSecretAuthentication;
use liberty_code\authentication\authentication\token\library\ConstTokenAuthentication;
use liberty_code\authentication\agent\model\DefaultAgent;



// Test agent
$tabTabConfig = array(
    [
        $objUserAgent,
        [
            ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER => 'usr_4',
            ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET => 'pw'
        ]
    ], // Ko: Identification ok, authentication ko

    [
        $objUserAgent,
        [
            ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER => 'usr',
            ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET => 'pw~4'
        ]
    ], // Ko: Identification ko, authentication ko

    [
        $objUserAgent,
        [
            ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER => 'usr_4',
            ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET => 'pw~4'
        ]
    ], // Ok

    [
        $objTokenAgent,
        [
            ConstTokenAuthentication::TAB_CONFIG_KEY_TOKEN => 'token'
        ]
    ], // Ko: Identification ko, authentication ko

    [
        $objTokenAgent,
        [
            ConstTokenAuthentication::TAB_CONFIG_KEY_TOKEN => 'token-5'
        ]
    ] // Ok
);

foreach($tabTabConfig as $tabConfig)
{
    echo('Test agent : <pre>');
    var_dump($tabConfig);
    echo('</pre>');

    /** @var DefaultAgent $objAgent */
    $objAgent = $tabConfig[0];
    $tabAuthConfig = $tabConfig[1];

    try{
        $objAgent->getObjAuthentication()->setAuthConfig($tabAuthConfig);
        $objAgent->removeCache();

        echo('Check identification: <pre>');
        var_dump($objAgent->checkIsIdentified());
        echo('</pre>');

        echo('Check authentication: <pre>');
        var_dump($objAgent->checkIsAuthenticated());
        echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


