<?php

namespace liberty_code\authentication\agent\test;

use liberty_code\authentication\agent\fix\model\FixAgent;

use liberty_code\authentication\authentication\token\model\TokenAuthentication;
use liberty_code\authentication\authenticator\test\TestAuthenticator;
use liberty_code\authentication\agent\library\ConstAgent;



/**
 * @method TokenAuthentication getObjAuthentication() @inheritdoc
 * @method TestAuthenticator getObjAuthenticator() @inheritdoc
 */
class TestTokenAgent extends FixAgent
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param TokenAuthentication $objAuthentication
     * @param TestAuthenticator $objAuthenticator
     */
    public function __construct(
        TokenAuthentication $objAuthentication,
        TestAuthenticator $objAuthenticator
    )
    {
        // Call parent constructor
        parent::__construct($objAuthentication, $objAuthenticator);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixAuthenticationClassPath()
    {
        // Return result
        return TokenAuthentication::class;
    }



    /**
     * @inheritdoc
     */
    protected function getStrFixAuthenticatorClassPath()
    {
        // Return result
        return TestAuthenticator::class;
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstAgent::TAB_CONFIG_KEY_KEY => 'token',
            //ConstAgent::TAB_CONFIG_KEY_CACHE_REQUIRE => true
        );
    }



}