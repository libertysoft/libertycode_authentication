<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/authenticator/test/AuthenticatorTest.php');
require_once($strRootAppPath . '/src/agent/test/TestUserAgent.php');
require_once($strRootAppPath . '/src/agent/test/TestTokenAgent.php');

// Use
use liberty_code\authentication\authentication\secret\model\SecretAuthentication;
use liberty_code\authentication\authentication\token\model\TokenAuthentication;
use liberty_code\authentication\agent\test\TestUserAgent;
use liberty_code\authentication\agent\test\TestTokenAgent;



// Init var
$objUserAgent = new TestUserAgent(
    new SecretAuthentication(),
    $objAuthenticator
);

$objTokenAgent = new TestTokenAgent(
    new TokenAuthentication(),
    $objAuthenticator
);


