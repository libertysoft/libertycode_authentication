<?php
/**
 * Description :
 * This class allows to describe behavior of authenticator class.
 * Authenticator is an authentication support,
 * to check identification and authentication,
 * from a specified authentication.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\authenticator\api;

use liberty_code\authentication\authentication\api\AuthenticationInterface;



interface AuthenticatorInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods check
	// ******************************************************************************

    /**
     * Check if specified authentication is identified.
     *
     * @param AuthenticationInterface $objAuthentication
     * @return boolean
     */
    public function checkIsIdentified(AuthenticationInterface $objAuthentication);



    /**
     * Check if specified authentication is authenticated.
     *
     * @param AuthenticationInterface $objAuthentication
     * @return boolean
     */
    public function checkIsAuthenticated(AuthenticationInterface $objAuthentication);





    // Methods getters
    // ******************************************************************************

    /**
     * Get authenticator configuration array.
     *
     * @return array
     */
    public function getTabAuthConfig();





    // Methods setters
    // ******************************************************************************

    /**
     * Set authenticator configuration array.
     *
     * @param array $tabConfig
     */
    public function setAuthConfig(array $tabConfig);
}