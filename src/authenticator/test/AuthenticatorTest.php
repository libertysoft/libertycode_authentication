<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/authenticator/test/TestAuthenticator.php');

// Use
use liberty_code\authentication\authenticator\test\TestAuthenticator;
use liberty_code\authentication\authentication\secret\library\ConstSecretAuthentication;
use liberty_code\authentication\authentication\token\library\ConstTokenAuthentication;



// Init var
$tabConfig = array(
    'mapping' => [
        ConstSecretAuthentication::TAB_DATA_KEY_IDENTIFIER => 'login',
        ConstSecretAuthentication::TAB_DATA_KEY_SECRET => 'password',
        ConstTokenAuthentication::TAB_DATA_KEY_TOKEN => 'token'
    ],
    'data' => [
        [
            'login' => 'usr_1',
            'password' => 'pw~1',
            'token' => 'token-1'
        ],
        [
            'login' => 'usr_2',
            'password' => 'pw~2',
            'token' => 'token-2'
        ],
        [
            'login' => 'usr_3',
            'password' => 'pw~3',
            'token' => 'token-3'
        ],
        [
            'login' => 'usr_4',
            'password' => 'pw~4',
            'token' => 'token-4'
        ],
        [
            'login' => 'usr_5',
            'password' => 'pw~5',
            'token' => 'token-5'
        ]
    ]
);
$objAuthenticator = new TestAuthenticator($tabConfig);


