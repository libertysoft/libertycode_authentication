<?php
/**
 * This class allows to define test authenticator class.
 *
 * Test authenticator uses the following specified configuration:
 * [
 *     mapping => [
 *         'authentication data key 1' => 'authenticator data key 1',
 *         ...,
 *         'authentication data key N' => 'authenticator data key N'
 *     ],
 *
 *     data => [
 *         // Data set 1
 *         [
 *             key 1 => 'value 1',
 *             ...,
 *             key N => 'value N'
 *         ],
 *         ...,
 *         // Data set N
 *         [...]
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\authenticator\test;

use liberty_code\authentication\authenticator\model\DefaultAuthenticator;

use liberty_code\authentication\authentication\api\AuthenticationInterface;



class TestAuthenticator extends DefaultAuthenticator
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified data is selected,
     * from specified selection data.
     *
     * @param array $data
     * @param array $tabSelectData
     * @return boolean
     */
    protected function checkDataIsSelected(array $data, array $tabSelectData)
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();
        $tabMap = $tabConfig['mapping'];
        $tabSelectKey = array_keys($tabSelectData);
        $result = (count($tabSelectData) > 0);

        // Run each select data
        for($intCpt = 0; ($intCpt < count($tabSelectKey)) && $result; $intCpt++)
        {
            $selectKey = $tabSelectKey[$intCpt];
            $selectData = $tabSelectData[$selectKey];
            $dataKey = (array_key_exists($selectKey, $tabMap) ? $tabMap[$selectKey] : null);

            // Check data selected
            $result = (
                (!is_null($dataKey)) &&
                array_key_exists($dataKey, $data) &&
                ($data[$dataKey] === $selectData)
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkIsIdentified(AuthenticationInterface $objAuthentication)
    {
        // Init var
        $tabIdData = $objAuthentication->getTabIdData();

        // Return result
        return is_array($this->getData($tabIdData));
    }



    /**
     * @inheritdoc
     */
    public function checkIsAuthenticated(AuthenticationInterface $objAuthentication)
    {
        // Init var
        $tabIdData = $objAuthentication->getTabIdData();
        $tabAuthData = $objAuthentication->getTabAuthData();
        $data = $this->getData($tabIdData);
        $result = (
            (!is_null($data)) &&
            $this->checkDataIsSelected($data, $tabAuthData)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get data,
     * from specified associative array of identification data.
     *
     * @param array $tabIdData
     * @return null|array
     */
    protected function getData(array $tabIdData)
    {
        // Init var
        $result = null;
        $tabConfig = $this->getTabAuthConfig();
        $tabData = $tabConfig['data'];

        // Run each data
        for($intCpt = 0; ($intCpt < count($tabData)) && is_null($result); $intCpt++)
        {
            // Init var
            $data = $tabData[$intCpt];

            // Register data, if selected
            if($this->checkDataIsSelected($data, $tabIdData))
            {
                $result = $data;
            }
        }

        // Return result
        return $result;
    }



}