<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/authenticator/test/AuthenticatorTest.php');

// Use
use liberty_code\authentication\authentication\api\AuthenticationInterface;
use liberty_code\authentication\authentication\secret\library\ConstSecretAuthentication;
use liberty_code\authentication\authentication\secret\model\SecretAuthentication;
use liberty_code\authentication\authentication\token\library\ConstTokenAuthentication;
use liberty_code\authentication\authentication\token\model\TokenAuthentication;



// Test authenticator
$tabAuthentication = array(
    new SecretAuthentication(array(
        ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER => 'usr_1',
        ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET => 'pw1'
    )), // Ko: Identification ok, authentication ko

    new SecretAuthentication(array(
        ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER => 'usr_2',
        ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET => 'pw~1'
    )), // Ko: Identification ok, authentication ko

    new SecretAuthentication(array(
        ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER => 'usr_2',
        ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET => 'pw~2'
    )), // Ok

    new TokenAuthentication(array(
        ConstTokenAuthentication::TAB_CONFIG_KEY_TOKEN => 'token_3'
    )), // Ko: Identification ko, authentication ko

    new TokenAuthentication(array(
        ConstTokenAuthentication::TAB_CONFIG_KEY_TOKEN => 'token-3'
    )) // Ok
);

foreach($tabAuthentication as $objAuthentication)
{
    /** @var AuthenticationInterface $objAuthentication */

    echo('Test authenticator : <pre>');
    var_dump($objAuthentication->getTabAuthConfig());
    echo('</pre>');

    try{
        echo('Check identification: <pre>');
        var_dump($objAuthenticator->checkIsIdentified($objAuthentication));
        echo('</pre>');

        echo('Check authentication: <pre>');
        var_dump($objAuthenticator->checkIsAuthenticated($objAuthentication));
        echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


