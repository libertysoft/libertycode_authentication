<?php
/**
 * This class allows to define default authenticator class.
 * Can be consider is base of all authenticator types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication\authenticator\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\authentication\authenticator\api\AuthenticatorInterface;

use liberty_code\authentication\authenticator\library\ConstAuthenticator;
use liberty_code\authentication\authenticator\exception\ConfigInvalidFormatException;



abstract class DefaultAuthenticator extends FixBean implements AuthenticatorInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     */
    public function __construct(array $tabConfig = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setAuthConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstAuthenticator::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstAuthenticator::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstAuthenticator::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAuthenticator::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabAuthConfig()
    {
        // Return result
        return $this->beanGet(ConstAuthenticator::DATA_KEY_DEFAULT_CONFIG);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setAuthConfig(array $tabConfig)
    {
        $this->beanSet(ConstAuthenticator::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



}