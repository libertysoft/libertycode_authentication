LibertyCode_Authentication
==========================



Description
-----------

Library contains authentication components, 
to manage identification and authentication system.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/authentication ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/authentication": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Authentication

Authentication provides identification and authentication information,
about one specific entity.

_Elements_

- Authentication

    Allows to design an authentication, 
    to get all identification and authentication information,
    about one specific entity.

- SecretAuthentication

    Extends authentication features. 
    Uses identifier, as identification information
    and secret, as authentication information.
    
- TokenAuthentication

    Extends authentication features. 
    Uses token, as identification and authentication information.
    
- CredentialAuthentication

    Extends authentication features.
    Uses credential to retrieve identifier, as identification 
    and secret, as authentication information.
    
_Example_

```php
// Get secret authentication
use liberty_code\authentication\authentication\secret\model\SecretAuthentication;
$secretAuth = new SecretAuthentication();
$secretAuth->setIdentifier(...);
$secretAuth->setSecret(...);
...
// Get array of identification data
var_dump($secretAuth->getTabIdData());
...
// Get array of authentication data
var_dump($secretAuth->getTabAuthData());
...
```

#### Authenticator

Authenticator is authentication support, 
allows to check identification and authentication, 
from a specified authentication.

_Elements_

- Authenticator

    Allows to design an authenticator, 
    to check identification and authentication, 
    from a specified authentication.

#### Agent

Agent allows to represent authentication about one specific entity,
to check its identification and its authentication.
 
_Elements_

- Agent

    Allows to design an agent, 
    to check identification and authentication, 
    about one specific entity.
    
- FixAgent

    Extends agent features. 
    Allows to check identification and authentication, 
    about one specific entity,
    from a specified fixed configuration.

_Example_

```php
// Get agent
use liberty_code\authentication\agent\model\DefaultAgent;
$agent= new DefaultAgent(
    $secretAuth, 
    ...(authenticator object)
);
...
// Check is identified
var_dump($agent->checkIsIdentified());
...
// Check is authenticated
var_dump($agent->checkIsAuthenticated());
...
```

---


